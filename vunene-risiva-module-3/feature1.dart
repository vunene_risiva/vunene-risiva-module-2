import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/loginpage.dart';

class profile_edit extends StatelessWidget
{
  const profile_edit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar:AppBar(
        title: Center (child:Text("Edit Profile"))
      ),

      body:Column(
        mainAxisAlignment:MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
          children:[ Text("User can edit the username and password"),
            TextField(obscureText: false,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'new username')),
            TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'old password')),
            TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'new password')),
            TextField(obscureText: true,decoration: InputDecoration(border: OutlineInputBorder(),labelText: 'confirm new password')),
            ElevatedButton(onPressed: ()=> {Navigator.push(context,
                MaterialPageRoute(builder:
                    (context) =>loginpage()))}, child: const Text("Login")),
            Container(alignment: Alignment.center)
          ]

      )

      );

  }
}
