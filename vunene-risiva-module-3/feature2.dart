import 'package:flutter/material.dart';

class help extends StatelessWidget
{
  const help({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
        appBar:AppBar(
            title: Center (child:Text("Help"))
        ),

        body:Column(
          mainAxisAlignment:MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children:
          [
            Text("1. The registration page allows the user to create an account",style: TextStyle(fontSize: 20.0)),
            Text("2.The profile edit page alows the user to be able to change the username and password of the user",style: TextStyle(fontSize: 20.0)),
            Text("3.The following page is the help page and this answers the user's questions",style: TextStyle(fontSize: 20.0)),
            Container(alignment: Alignment.center,)
          ],

        )
    );
  }
}
