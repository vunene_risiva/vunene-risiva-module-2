
class MTNApp
{
  String name;
  String sector;
  String developer;
  int year;

  MTNApp(String name,String sector,String developer,int year)
  {
    this.name=name;
    this.sector=sector;
    this.developer=developer;
    this.year=year;
  }

  String capitalize()
  {
    String cap_name=this.name.toUpperCase();
    return(cap_name);
  }
  void printDetails()
  {
    print("Name of the app:  $name  Sector:  $sector  Developer: $developer");

  }

}


void main(List<String> arguments)
{
  MTNApp ambani_africa=new MTNApp("Ambani Africa", "Gaming", "Mukundi Lambani", 2021);
  print(ambani_africa.capitalize());
  ambani_africa.printDetails();

}

